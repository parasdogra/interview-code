from code import solution

def test_generate_password():
# We can use if/else statement but in the description assert function was mentioned so I used assert statement
# This test case represnt the complexity is 2 and length is 6. We assert that complexity should be 2
    password = solution.generate_password(6,2)
    try:
        assert solution.check_password_level(password) == 2
        print("success")
    except:
        print("Failure")
# This case represent when complexity is 2 and assert that complexity override the expection
    password_1 = solution.generate_password(8,2)
    try:
        assert solution.check_password_level(password_1) == 3
        print("success")
    except:
        print("Failure")
# This case represent when complexity 1 and length of password is 8. Test Case of checking complexity level exception 
    password_2 = solution.generate_password(8, 1)
    try:
        assert solution.check_password_level(password_2) != 1
        print("success")
    except:
        print("Failure")
# This test case represent the complexity 4 and length of password is 7. Test case to check complexity level 4
    password_3 = solution.generate_password(7, 4)
    try:
        assert solution.check_password_level(password_3) == 4
        print("success")
    except:
        print("Failure")

test_generate_password()
