import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string  # https://docs.python.org/3.6/library/string.html
import requests
import json

def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

    Complexity levels:
        Complexity == 1: return a password with only lowercase chars
        Complexity ==  2: Previous level plus at least 1 digit
        Complexity ==  3: Previous levels plus at least 1 uppercase char
        Complexity ==  4: Previous levels plus at least 1 punctuation char

    :param length: number of characters
    :param complexity: complexity level
    :returns: generated password
    """
    mandatory_chr = ""
    options = string.printable[10:36]

    if complexity == 2:
        options = string.printable[:36]
        mandatory_chr = str(random.randint(0, 10))
    elif complexity == 3:
        options = string.printable[:62]
        mandatory_chr = str(random.randint(0, 9)) + string.printable[random.randint(36,61)]
    elif complexity == 4:
        options = string.printable[0:94]
        mandatory_chr = str(random.randint(0, 9)) + string.printable[random.randint(36,61)] + string.printable[random.randint(62, 93)]

    password = []

    while len(password) < length:
        password.append(options[random.randint(0, len(options))-1])

    visited_index = set()   # set to make sure we don't overwrite the same index
    for ch in mandatory_chr:
        ind = random.randint(0, length-1)
        while ind in visited_index:
            ind = random.randint(0, length-1)
        password[ind] = ch
        visited_index.add(ind)

    return "".join(password)


def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

    Complexity levels:
        Return complexity 1: If password has only lowercase chars
        Return complexity 2: Previous level condition and at least 1 digit
        Return complexity 3: Previous levels condition and at least 1 uppercase char
        Return complexity 4: Previous levels condition and at least 1 punctuation

    Complexity level exceptions (override previous results):
        Return complexity 2: password has length >= 8 chars and only lowercase chars
        Return complexity 3: password has length >= 8 chars and only lowercase and digits

    :param password: password
    :returns: complexity level
    """
    has_digit = 0
    has_upper_case = 0
    has_punctuation = 0

    for i in range(len(password)):
        if password[i].isdigit():
            has_digit = 1
        elif password[i].isupper():
            has_upper_case = 1
        elif password[i] in string.punctuation:
            has_punctuation = 1

    complexity = has_digit + has_upper_case + has_punctuation + 1

    # increment complexity if complexity level exception exists i.e. len >= 8
    if complexity <= 2 and len(password) >= 8:
        complexity += 1

    return complexity



def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
    :return: None
    """
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()

    try:
        cur.execute("CREATE TABLE User ( firstname TEXT, lastname TEXT, "
                    "password TEXT,email TEXT PRIMARY KEY)")
    except:
        # db already exists
        pass

    res = requests.get("https://randomuser.me/api/")
    user = json.loads(res.text)
    email = user["results"][0]["email"]

    # query to check if random user already exists in User table or not
    db_query = cur.execute("Select * from User where email='{}'".format(email))

    # request API until we get a  user that does not exists in User table
    while db_query.fetchone():
        res = requests.get("https://randomuser.me/api/")
        user = json.loads(res.text)
        email = user["results"][0]["email"]

    firstname = user["results"][0]["name"]["first"]
    lastname = user["results"][0]["name"]["last"]
    complexity = random.randint(1, 4)
    password = generate_password(random.randint(6, 12), complexity)   # generating a password of length bw 6 to 12

    cur.execute('''INSERT INTO User (firstname, lastname, password, email) values (?, ?, ?, ?)''',
                (firstname, lastname, password, email))   # executing the query to add user

    conn.commit()
    conn.close()    # closing the connection
